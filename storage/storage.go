package storage

import (
	pb "catalog_service/genproto/catalog_service"
	"context"
	"google.golang.org/protobuf/types/known/emptypb"
)

type IStorage interface {
	Close()
	Category() ICategoryStorage
}

type ICategoryStorage interface {
	Create(context.Context, *pb.CreateCategoryRequest) (*pb.Category, error)
	Get(context.Context, *pb.PrimaryKey) (*pb.Category, error)
	GetList(context.Context, *pb.GetListRequest) (*pb.CategoriesResponse, error)
	Update(context.Context, *pb.Category) (*pb.Category, error)
	Delete(context.Context, *pb.PrimaryKey) (*emptypb.Empty, error)
}
