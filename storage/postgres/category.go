package postgres

import (
	pb "catalog_service/genproto/catalog_service"
	"catalog_service/pkg/helper"
	"catalog_service/pkg/logger"
	"catalog_service/storage"
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type categoryRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewCategoryRepo(db *pgxpool.Pool, log logger.ILogger) storage.ICategoryStorage {
	return &categoryRepo{
		db:  db,
		log: log,
	}
}

func (h *categoryRepo) Create(ctx context.Context, request *pb.CreateCategoryRequest) (*pb.Category, error) {
	var (
		category                  = pb.Category{}
		err                       error
		title                     []byte
		titleStr, searchingColumn string
	)

	query := `
		insert into categories (id, title, status, parent_id, order_number, searching_column)
			values ($1, $2, $3, $4, $5, $6)
			returning id, title, status, parent_id, order_number`

	title, err = json.Marshal(request.Titel)
	if err != nil {
		fmt.Println("err 1", err.Error())
		return nil, err
	}

	searchingColumn = fmt.Sprintf("%s %v %s %d %s %s", title, request.Active, request.PerentId, request.OrderNumber)

	if err = h.db.QueryRow(ctx, query,
		uuid.New().String(),
		string(title),
		request.Active,
		request.PerentId,
		request.OrderNumber,
		searchingColumn).
		Scan(
			&category.Id,
			&titleStr,
			&category.Active,
			&category.PerentId,
			&category.OrderNumber,
		); err != nil {
		fmt.Println("err 3", err.Error())
		return nil, err
	}

	if err = json.Unmarshal([]byte(titleStr), &category.Titel); err != nil {
		fmt.Println("err 4", err.Error())
		return nil, err
	}

	return &category, nil
}

func (h *categoryRepo) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Category, error) {
	var (
		category = pb.Category{}
		title    string
	)

	query := `select id, title, status, parent_id, order_number from categories where deleted_at = 0 and id = $1`

	if err := h.db.QueryRow(ctx, query, key.GetId()).Scan(
		&category.Id,
		&title,
		&category.Active,
		&category.PerentId,
		&category.OrderNumber,
	); err != nil {
		h.log.Error("error while scanning category by id", logger.Error(err))
		return nil, err
	}

	if err := json.Unmarshal([]byte(title), &category.Titel); err != nil {
		h.log.Error("error while unmarshalling category title", logger.Error(err))
		return nil, err
	}

	return &category, nil
}

func (h *categoryRepo) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.CategoriesResponse, error) {
	var (
		resp   = pb.CategoriesResponse{}
		filter = "where deleted_at = 0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s' ", request.GetSearch())
	}

	countQuery := `select count(1) from categories ` + filter

	if err := h.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		h.log.Error("error while scanning count of categories", logger.Error(err))
		return nil, err
	}

	query := `select id, title, status, parent_id, order_number from categories ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := h.db.Query(ctx, query)
	if err != nil {
		h.log.Error("error while getting category rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			category = pb.Category{}
			title    string
		)

		if err = rows.Scan(
			&category.Id,
			&title,
			&category.Active,
			&category.PerentId,
			&category.OrderNumber,
		); err != nil {
			h.log.Error("error while scanning ony by one category", logger.Error(err))
			return nil, err
		}

		if err = json.Unmarshal([]byte(title), &category.Titel); err != nil {
			h.log.Error("error while unmarshalling title of category", logger.Error(err))
			return nil, err
		}

		resp.Categories = append(resp.Categories, &category)
	}

	resp.Count = count

	return &resp, nil
}

func (h *categoryRepo) Update(ctx context.Context, category *pb.Category) (*pb.Category, error) {
	var (
		resp   = pb.Category{}
		params = make(map[string]interface{})
		query  = `update categories set `
		filter = ""
	)

	params["id"] = category.GetId()
	fmt.Println("category id", category.GetId())

	if category.GetTitel() != "" {
		jsName, err := json.Marshal(category.GetTitel())
		if err != nil {
			return nil, err
		}

		params["name"] = string(jsName)

		filter += " name = @name,"
	}

	if category.GetActive() {
		params["status"] = category.GetActive()

		filter += " status = @status,"
	}

	if category.GetPerentId() != "" {
		params["parent_id"] = category.GetPerentId()

		filter += " parent_id = @parent_id,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, phone`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := h.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
		&resp.Titel,
		&resp.Active,
		&resp.PerentId,
		&resp.OrderNumber,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (h *categoryRepo) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	_, err := h.db.Exec(ctx, `update categories set deleted_at = extract(epoch from current_timestamp) where id = $1`, key.GetId())

	return nil, err
}
