// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v4.25.3
// source: delivery_tarif_service.proto

package order_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	DelivryTarifService_Create_FullMethodName  = "/order_service.DelivryTarifService/Create"
	DelivryTarifService_Get_FullMethodName     = "/order_service.DelivryTarifService/Get"
	DelivryTarifService_GetList_FullMethodName = "/order_service.DelivryTarifService/GetList"
	DelivryTarifService_Update_FullMethodName  = "/order_service.DelivryTarifService/Update"
	DelivryTarifService_Delete_FullMethodName  = "/order_service.DelivryTarifService/Delete"
)

// DelivryTarifServiceClient is the client API for DelivryTarifService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type DelivryTarifServiceClient interface {
	Create(ctx context.Context, in *CreateDelivryTarifRequest, opts ...grpc.CallOption) (*DelivryTarif, error)
	Get(ctx context.Context, in *PrimaryKey, opts ...grpc.CallOption) (*DelivryTarif, error)
	GetList(ctx context.Context, in *GetListRequest, opts ...grpc.CallOption) (*DelivryTarifsResponse, error)
	Update(ctx context.Context, in *DelivryTarif, opts ...grpc.CallOption) (*DelivryTarif, error)
	Delete(ctx context.Context, in *PrimaryKey, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type delivryTarifServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewDelivryTarifServiceClient(cc grpc.ClientConnInterface) DelivryTarifServiceClient {
	return &delivryTarifServiceClient{cc}
}

func (c *delivryTarifServiceClient) Create(ctx context.Context, in *CreateDelivryTarifRequest, opts ...grpc.CallOption) (*DelivryTarif, error) {
	out := new(DelivryTarif)
	err := c.cc.Invoke(ctx, DelivryTarifService_Create_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *delivryTarifServiceClient) Get(ctx context.Context, in *PrimaryKey, opts ...grpc.CallOption) (*DelivryTarif, error) {
	out := new(DelivryTarif)
	err := c.cc.Invoke(ctx, DelivryTarifService_Get_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *delivryTarifServiceClient) GetList(ctx context.Context, in *GetListRequest, opts ...grpc.CallOption) (*DelivryTarifsResponse, error) {
	out := new(DelivryTarifsResponse)
	err := c.cc.Invoke(ctx, DelivryTarifService_GetList_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *delivryTarifServiceClient) Update(ctx context.Context, in *DelivryTarif, opts ...grpc.CallOption) (*DelivryTarif, error) {
	out := new(DelivryTarif)
	err := c.cc.Invoke(ctx, DelivryTarifService_Update_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *delivryTarifServiceClient) Delete(ctx context.Context, in *PrimaryKey, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, DelivryTarifService_Delete_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DelivryTarifServiceServer is the server API for DelivryTarifService service.
// All implementations must embed UnimplementedDelivryTarifServiceServer
// for forward compatibility
type DelivryTarifServiceServer interface {
	Create(context.Context, *CreateDelivryTarifRequest) (*DelivryTarif, error)
	Get(context.Context, *PrimaryKey) (*DelivryTarif, error)
	GetList(context.Context, *GetListRequest) (*DelivryTarifsResponse, error)
	Update(context.Context, *DelivryTarif) (*DelivryTarif, error)
	Delete(context.Context, *PrimaryKey) (*emptypb.Empty, error)
	mustEmbedUnimplementedDelivryTarifServiceServer()
}

// UnimplementedDelivryTarifServiceServer must be embedded to have forward compatible implementations.
type UnimplementedDelivryTarifServiceServer struct {
}

func (UnimplementedDelivryTarifServiceServer) Create(context.Context, *CreateDelivryTarifRequest) (*DelivryTarif, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedDelivryTarifServiceServer) Get(context.Context, *PrimaryKey) (*DelivryTarif, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Get not implemented")
}
func (UnimplementedDelivryTarifServiceServer) GetList(context.Context, *GetListRequest) (*DelivryTarifsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedDelivryTarifServiceServer) Update(context.Context, *DelivryTarif) (*DelivryTarif, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedDelivryTarifServiceServer) Delete(context.Context, *PrimaryKey) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedDelivryTarifServiceServer) mustEmbedUnimplementedDelivryTarifServiceServer() {}

// UnsafeDelivryTarifServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to DelivryTarifServiceServer will
// result in compilation errors.
type UnsafeDelivryTarifServiceServer interface {
	mustEmbedUnimplementedDelivryTarifServiceServer()
}

func RegisterDelivryTarifServiceServer(s grpc.ServiceRegistrar, srv DelivryTarifServiceServer) {
	s.RegisterService(&DelivryTarifService_ServiceDesc, srv)
}

func _DelivryTarifService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateDelivryTarifRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DelivryTarifServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: DelivryTarifService_Create_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DelivryTarifServiceServer).Create(ctx, req.(*CreateDelivryTarifRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DelivryTarifService_Get_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DelivryTarifServiceServer).Get(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: DelivryTarifService_Get_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DelivryTarifServiceServer).Get(ctx, req.(*PrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

func _DelivryTarifService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DelivryTarifServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: DelivryTarifService_GetList_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DelivryTarifServiceServer).GetList(ctx, req.(*GetListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DelivryTarifService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DelivryTarif)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DelivryTarifServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: DelivryTarifService_Update_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DelivryTarifServiceServer).Update(ctx, req.(*DelivryTarif))
	}
	return interceptor(ctx, in, info, handler)
}

func _DelivryTarifService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DelivryTarifServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: DelivryTarifService_Delete_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DelivryTarifServiceServer).Delete(ctx, req.(*PrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// DelivryTarifService_ServiceDesc is the grpc.ServiceDesc for DelivryTarifService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var DelivryTarifService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "order_service.DelivryTarifService",
	HandlerType: (*DelivryTarifServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _DelivryTarifService_Create_Handler,
		},
		{
			MethodName: "Get",
			Handler:    _DelivryTarifService_Get_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _DelivryTarifService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _DelivryTarifService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _DelivryTarifService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "delivery_tarif_service.proto",
}
